## JavaScript语言的词法规则和语法规则
* 词法规则
1. 注释：//... 或 /* ... */
2. 字符串："...", '...', "...\(\n)...", '...\(\n)...'，暂时不考虑字符串中的转义符
3. 数字：[+-]?\d+\.?\d* 或其他方式，暂时只考虑普通10进制数
4. 标示符：[\w\d_]+ 不考虑太复杂的标示符，包含关键字
5. 运算符：+ - * / ^ ! = == === ? : < > . != !==
6. 括号：[], (), {}
7. 其他符号: ;

* 语法规则
1. a 运算符 b
2. ! a
3. a.b
4. a[b]
5. a,b,c (参数列表，数组字面量等都包含)
6. a(b) (函数调用)
7. a;
8. return a, return
9. {a b c ... } 代码块
10. if(a) b else c
11. if(a) b
12. while(a) b, for(a in b) c, for(a?;b?;c?) d
13. do(a) while(b)
14. break a(a只能是一个标示符)
15. a = b (下面这几个基本语句的优先级要提高到上面去)
16. a == b
17. a != b , a !== b, a < b, a <= b, a > b, a >= b, ++a, a++ a+=b, a-=b, a*=b 等
18. var a, var a = b
19. new a(b), new a, delete a[b], delete a.b, delete a
20. function (a) { b }, function a (b) { c }
21. try a catch b finally c, try a finally b, try a catch b
100. (a) 最后