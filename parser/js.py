# coding: UTF-8
from __future__ import print_function

tokens = (
    'COMMENT', 'STRING', 'BOOLEAN', 'NUMBER', 'REGEX',
    'VAR', 'FUNCTION', 'RETURN', 'FOR', 'WHILE', 'SWITCH', 'CASE', 'BREAK', 'CONTINUE', 'IF', 'ELSE', 'THROW',
    'NEW', 'DELETE', 'TRY', 'CATCH', 'FINALLY', 'DEFAULT',
    'LEFT_XKH', 'RIGHT_XKH',
    'LEFT_ZKH', 'RIGHT_ZKH',
    'LEFT_DKH', 'RIGHT_DKH',
    'SEMICOLON', 'POINT', 'COMMA', 'COLON',
    'RIGHT_OPERATOR', # right operator is like !aVar
    'ASSIGN', 'BINARY_OPERATOR',
    'IDENTIFIER',
    'NEWLINE',
)
t_RIGHT_XKH = r'\)'
t_LEFT_ZKH = r'\['
t_RIGHT_ZKH = r'\]'
t_LEFT_DKH = r'\{'
t_POINT = r'\.'
t_COMMA = r','
t_COLON = r':'

import js_types as types


def t_RIGHT_DKH(t):
    r'}'
    # TODO
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_THROW(t):
    r'throw'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_NEW(t):
    r'new'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_DELETE(t):
    r'delete'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_VAR(t):
    r'var'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_FUNCTION(t):
    r'function'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_RETURN(t):
    r'return'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_FOR(t):
    r'for'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_IF(t):
    r'if'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_DEFAULT(t):
    r'default'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_ELSE(t):
    r'else'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_TRY(t):
    r'try'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_CATCH(t):
    r'catch'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_FINALLY(t):
    r'finally'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_WHILE(t):
    r'while'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_SWITCH(t):
    r'switch'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_CASE(t):
    r'case'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_BREAK(t):
    r'break'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_CONTINUE(t):
    r'continue'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_COMMENT(t):
    r'(//[^\n]*)|(/\*(\s|.)*?\*/)'
    t.lexer.lineno += t.value.count('\n')
    t.value = types.JsComment(t.value, t.lineno, t.lexpos)
    #print(t.value, t.lineno, t.lexpos)
    return t


def t_LEFT_XKH(t):
    r'\('
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_SEMICOLON(t):
    r';'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_ASSIGN(t):
    r'='
    t.value = types.JsAssignOperator(t.value, t.lineno, t.lexpos)
    #print(t.value, t.lineno, t.lexpos)
    return t


def t_REGEX(t):
    r'/(([^/])|(\/))+?/\w?'
    t.value = types.JsRegex(t.value, t.lineno, t.lexpos)
    return t


def t_RIGHT_OPERATOR(t):
    r'!|(\+\+)|(--)'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_BINARY_OPERATOR(t):
    r'[+\-*/<>|%^]|(&&)|(\|\|)|(&)|(\|)'
    t.value = types.JsCalcOperator(t.value, t.lineno, t.lexpos)
    #print(t.value, t.lineno, t.lexpos)
    return t


def t_STRING(t):
    r"(?:\"((?:\\.|[^\"])*)\"|'((?:\\.|[^'])*)')"
    t.value = types.JsString(t.value, t.lineno, t.lexpos)
    #print(t.value, t.lineno, t.lexpos)
    return t


def t_BOOLEAN(t):
    r'(true)|(false)'
    if t.value == 'true':
        t.value = True
    else:
        t.value = False
    t.value = types.JsBoolean(t.value, t.lineno, t.lexpos)
    #print(t.value, t.lineno, t.lexpos)
    return t


def t_NUMBER(t):
    r'[+-]?(?:(0x[0-9a-fA-F]+|0[0-7]+)|((?:[0-9]+(?:\.[0-9]*)?|\.[0-9]+)(?:[eE][+-]?[0-9]+)?|NaN|Infinity))'
    source_text = t.value
    try:
        if t.value.count('.') > 0:
            t.value = float(t.value)
        else:
            t.value = long(t.value)
    except ValueError:
        print("Too large number", t.value)
        t.value = 0
    t.value = types.JsNumber(source_text, t.value, t.lineno, t.lexpos)
    #print(t.value, t.lineno, t.lexpos)
    return t


def t_IDENTIFIER(t):
    r'[a-zA-Z_\$]*[\w\$]+'
    if t.value in types.JsKeyword.keywords:
        t.value = types.JsKeyword(t.value, t.lineno, t.lexpos)
    else:
        t.value = types.JsIdentity(t.value, t.lineno, t.lexpos)
        #print(t.value, t.lineno, t.lexpos)
    return t


def t_NEWLINE(t):
    r'(\r\n|\n)+'
    t.lexer.lineno += t.value.count('\n')
    t.value = types.JsNewLine(t.value, t.lineno, t.lexpos)
    return t


def t_SPACE(t):
    r'[\t ]+'
    #t.value = ' '
    #return t


def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


import ply.lex as lex


precedence = (
    ('left', 'COMMENT', 'NEWLINE',),
    ('left', 'BINARY_OPERATOR',),
    ('right', 'RIGHT_OPERATOR',),
    ('left', 'FOR', 'LEFT_XKH',),
    ('left', 'POINT', 'LEFT_XKH',),
    ('left', 'FUNCTION', 'LEFT_XKH',),
    ('right', 'SEMICOLON', 'RETURN',),
    ('right', 'RIGHT_XKH', 'RIGHT_ZKH', 'RIGHT_DKH',),
    ('left', 'LEFT_XKH', 'RIGHT_ZKH', 'RIGHT_DKH',),
)
identifiers = {}


def p_block2(p):
    """
    block : block statement
            | block final_statement
            | statement
            | final_statement
            | block error
    """
    if len(p) == 2 and p[1]:
        p[0] = types.JsBlock(items=[p[1]])
    else:
        p[0] = p[1]
        if p[0] and isinstance(p[0], types.JsBlock):
            p[0].add(p[2])
        else:
            p[0] = types.JsBlock(p[1:], 'block')


def p_block_error(p):
    """block : error"""
    p[0] = None
    p.parser.error = 1


def p_newline(p):
    """newline : NEWLINE
                | COMMENT
                | newline newline"""
    p[0] = p[1]


def p_block_newline(p):
    """
    block : block newline
        | newline block
    """
    if isinstance(p[1], types.JsNewLine):
        p[0] = p[2]
    else:
        p[0] = p[1]


def p_const_expression(p):
    """const_expression : NUMBER
                | STRING
                | BOOLEAN
                | REGEX"""
    p[0] = p[1]
    #print("this is an expression ", p[0])


def p_statement_single2(p):
    """expression : const_expression"""
    p[0] = p[1]


def p_declare_expr(p):
    """
    declare_expression : VAR IDENTIFIER
    """
    p[0] = types.JsExpression(p[1:], 'declare_expression')
    print('declare expression ', p[0])


def p_statement_identifier(p):
    """expression : IDENTIFIER"""
    p[0] = p[1]


def p_binary_operator_expr(p):
    """expression : expression BINARY_OPERATOR expression BINARY_OPERATOR expression BINARY_OPERATOR expression BINARY_OPERATOR expression
                | expression BINARY_OPERATOR expression BINARY_OPERATOR expression BINARY_OPERATOR expression
                | expression BINARY_OPERATOR expression BINARY_OPERATOR expression
                | expression BINARY_OPERATOR expression
    """
    p[0] = types.JsExpression(p[1:], 'binary_operate_expression')
    print('binary expression ', p[0])


def p_right_operator_expr(p):
    """expression : RIGHT_OPERATOR expression
                | BINARY_OPERATOR expression
    """
    p[0] = types.JsExpression([p[1], p[2]], 'right_operate_expression')
    print('right expression ', p[0])


def p_assign_expression(p):
    """
    assign_expression : declare_expression ASSIGN expression
                | IDENTIFIER ASSIGN expression
    """
    p[0] = types.JsExpression(p[1:], 'assign expression')
    print('assign expression ', p[0])


def p_js_obj1(p):
    """
    js_obj : IDENTIFIER
            | const_expression
            | js_obj POINT js_obj
            | js_obj LEFT_ZKH js_obj RIGHT_ZKH
            | LEFT_DKH RIGHT_DKH
    """
    # TODO
    p[0] = types.JsObject(p[1:], 'obj')
    #print('object of any kind')


def p_return1(p):
    """
    statement : RETURN statement
    """
    p[0] = types.JsReturnStatement(p[1:], 'return statement')


def p_return2(p):
    """
    final_statement : RETURN final_statement
                | RETURN statement SEMICOLON
    """
    p[0] = types.JsReturnStatement(p[1:], 'return final statement')


def p_js_obj_list1(p):
    """
    js_obj_list : js_obj
                | js_obj COMMA js_obj_list
                | js_obj_list COMMA
    """
    p[0] = types.JsObjectList(p[1:], 'obj list like params')
    #print('js obj list like params')


def p_dkh_block(p):
    """
    dkh_block : LEFT_DKH block RIGHT_DKH
    """
    p[0] = types.JsExpression(p[1:], '{block}')


def p_xkh_expression(p):
    """
    expression : LEFT_XKH expression RIGHT_XKH
    """
    p[0] = p[2]


def p_func_declare_is_expression(p):
    """
    expression : func_declare
    """
    p[0] = p[1]


def p_func_declare(p):
    """
    func_declare : FUNCTION IDENTIFIER LEFT_XKH js_obj_list RIGHT_XKH dkh_block
            | FUNCTION IDENTIFIER LEFT_XKH js_obj_list RIGHT_XKH statement
            | FUNCTION IDENTIFIER LEFT_XKH js_obj_list RIGHT_XKH final_statement
            | FUNCTION LEFT_XKH js_obj_list RIGHT_XKH dkh_block
            | FUNCTION LEFT_XKH js_obj_list RIGHT_XKH statement
            | FUNCTION LEFT_XKH js_obj_list RIGHT_XKH final_statement
    """
    # TODO
    p[0] = types.JsFuncDef(p[1:], 'func def')
    print('func def', p[1:])


def p_func_call_expression(p):
    """
    func_call_expression : js_obj LEFT_XKH js_obj_list RIGHT_XKH
    """
    p[0] = types.JsFuncCall(p[1:], 'func call')
    #print('js func call')


def p_switch_is_block(p):
    """
    block : switch
    """
    p[0] = p[1]


def p_switch(p):
    """
    switch : SWITCH LEFT_XKH expression RIGHT_XKH dkh_block
    """
    p[0] = types.JsSwitch(p[1:], 'switch')


def p_switch_body(p):
    """
    expression : CASE expression COLON dkh_block
            | CASE expression COLON final_statement
            | CASE expression COLON statement
            | DEFAULT COLON dkh_block
            | DEFAULT COLON final_statement
            | DEFAULT COLON statement
            | BREAK
    """
    # TODO
    p[0] = types.JsExpression(p[1:], 'switch_body')


def p_for_is_block(p):
    """
    block : for_block
    """
    p[0] = p[1]


def p_for(p):
    """
    for_block : FOR for_head_block dkh_block
            | FOR for_head_block final_statement
            | FOR for_head_block statement
    """
    p[0] = types.JsFor(p[1:], 'for')
    print('for block', p[0])


def p_xkh_block(p):
    """
    for_head_block : LEFT_XKH final_statement final_statement statement RIGHT_XKH
                | LEFT_XKH final_statement final_statement RIGHT_XKH
    """
    p[0] = types.JsForBlockHead(p[1:], 'for_head_block')


def p_while_is_expression(p):
    """
    block : while
    """
    p[0] = p[1]


def p_while(p):
    """
    while : WHILE LEFT_XKH expression RIGHT_XKH dkh_block
        | WHILE LEFT_XKH expression RIGHT_XKH final_statement
        | WHILE LEFT_XKH expression RIGHT_XKH statement
    """
    p[0] = types.JsWhile(p[1:], 'while')
    print('while ', p[0])


def p_if_is_expression(p):
    """
    block : if
    """
    p[0] = p[1]


def p_if(p):
    """
    if : if_body else_body
        | if_body
    """
    p[0] = types.JsIf(p[1:], 'if')


def p_if_body(p):
    """
    if_body : IF LEFT_XKH expression RIGHT_XKH dkh_block
            | IF LEFT_XKH expression RIGHT_XKH final_statement
            | IF LEFT_XKH expression RIGHT_XKH statement
    """
    p[0] = types.JsBlock(p[1:], 'if_body')


def p_else_body(p):
    """
    else_body : ELSE dkh_block
            | ELSE final_statement
            | ELSE statement
    """
    p[0] = types.JsBlock(p[1:], 'else_body')


def p_try_catch_finally_is_block(p):
    """
    block : try_catch_finally
    """
    p[0] = p[1]


def p_try_catch_finally(p):
    """
    try_catch_finally : try_body catch_body finally_body
                    | try_body finally_body
    """
    p[0] = types.JsTryCatchFinally(p[1:], 'try_catch_finally')


def p_try_body(p):
    """
    try_body : TRY dkh_block
            | TRY final_statement
            | TRY statement
    """
    p[0] = types.JsExpression(p[1:], 'try_body')


def p_catch_body(p):
    """
    catch_body : CATCH LEFT_XKH expression RIGHT_XKH dkh_block
            | CATCH LEFT_XKH RIGHT_XKH dkh_block
            | CATCH LEFT_XKH expression RIGHT_XKH final_statement
            | CATCH LEFT_XKH RIGHT_XKH final_statement
            | CATCH LEFT_XKH expression RIGHT_XKH statement
            | CATCH LEFT_XKH RIGHT_XKH statement
    """
    p[0] = types.JsExpression(p[1:], 'catch_body')


def p_finally_body(p):
    """
    finally_body : FINALLY dkh_block
                | FINALLY final_statement
                | FINALLY statement
    """
    p[0] = types.JsExpression(p[1:], 'finally_body')


def p_js_obj_of_func_call(p):
    """
    js_obj : func_call_expression
    """
    p[0] = p[1]


def p_js_obj_is_expression(p):
    """
    expression : js_obj
    """
    p[0] = p[1]


def p_new(p):
    """
    expression : NEW expression
    """
    p[0] = types.JsExpression(p[1:], 'new_expression')


def p_delete(p):
    """
    expression : DELETE expression
    """
    p[0] = types.JsExpression(p[1:], 'delete_expression')


def p_throw(p):
    """
    expression : THROW expression
    """
    p[0] = types.JsExpression(p[1:], 'throw_expression')


def p_statement(p):
    """
    statement : expression
    | declare_expression
    | assign_expression
    | statement newline
    | newline statement
    """
    p[0] = types.JsStatement(p[1])
    #print('statement ', p[0])


def p_final_statement(p):
    """
    final_statement : statement SEMICOLON
                    | final_statement SEMICOLON
    """
    if isinstance(p[1], types.JsFinalStatement):
        p[0] = p[1]
    else:
        p[0] = types.JsFinalStatement(p[1])
    print('final statement ', p[0])


def p_single_semicolon(p):
    """
    final_statement : SEMICOLON
    """
    p[0] = p[1]
    print('single ;')


def p_break(p):
    """
    statement : BREAK
    """
    p[0] = p[1]


def p_continue(p):
    """
    statement : CONTINUE
    """
    p[0] = p[1]


def p_error(p):
    if p:
        print("Syntax error at '%s'" % p.value)
        print(p.lineno, p.lexpos, p.type)
    else:
        print("Syntax error at EOF")
        ## Read ahead looking for a closing '}'
        #while 1:
        #    tok = yacc.token()             # Get the next token
        #    if not tok or tok.type == 'RBRACE': break
        #yacc.restart()


import ply.yacc as yacc


def run_lex(text):
    lex.lex()
    lex.input(text)
    while True:
        token = lex.token()
        if not token:
            break
        print(token)


def run_yacc(text):
    lex.lex()
    yacc.yacc()
    result = yacc.parse(text)
    return result

