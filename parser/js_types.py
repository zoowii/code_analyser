# coding: UTF-8
from __future__ import print_function
from core.types import GObject
from core import types


class JsString(GObject):
    type = 'string'

    def __init__(self, source_text, lineno, lexpo):
        GObject.__init__(self, source_text, lineno, lexpo)
        # TODO: escape the string \
        self.add_type_meta_info(types.GTypes.STRING)


class JsBoolean(GObject):
    type = 'boolean'

    def __init__(self, val, lineno, lexpos):
        GObject.__init__(self, None, lineno, lexpos)
        self.value = val
        self.source_text = None
        self.add_type_meta_info(types.GTypes.BOOL)


class JsNumber(GObject):
    type = 'number'

    def __init__(self, source_text, val, lineno, lexpos):
        GObject.__init__(self, source_text, lineno, lexpos)
        self.source_text = source_text
        self.value = val
        self.lineno = lineno
        self.lexpos = lexpos
        self.add_type_meta_info(types.GTypes.NUMBER)


class JsRegex(GObject):
    type = 'regex'

    def __init__(self, source_text, lineno, lexpos):
        GObject.__init__(self, source_text, lineno, lexpos)
        self.source_text = source_text
        self.value = source_text
        self.lineno = lineno
        self.lexpos = lexpos
        self.add_type_meta_info(types.GTypes.REGEX)


class JsIdentity(GObject):
    type = 'identity'


class JsKeyword(GObject):
    type = 'keyword'
    keywords = ['var', 'function' 'for', 'while', 'return', 'try', 'catch', 'break', 'switch', 'case', 'throw', 'do',
                'while', 'null', 'undefined']


class JsOperator(GObject):
    type = 'operator'


class JsCalcOperator(JsOperator):
    type = 'calc_operator'
    all_items = ['+', '-', '*', '/', '<', '>', '!', '|', '%', '^']
    regex_rule = r'[+\-*/<>!|%^]'


class JsAssignOperator(JsOperator):
    type = 'assign'


class JsSpace(GObject):
    type = 'space'


class JsComment(GObject):
    type = 'comment'
    instances = []

    @staticmethod
    def add_instance(comment):
        JsComment.instances.append(comment)

    @staticmethod
    def init():
        JsComment.instances = []


class JsExpression(GObject):
    type = 'expression'
    items = []

    def __init__(self, items, type):
        self.init_object()
        self.items = items
        self.type = type
        self.has_got_definitions = False

    def add(self, item):
        self.items.append(item)

    def insert(self, index, item):
        self.items.insert(index, item)

    def unshift(self, item):
        self.insert(0, item)

    @property
    def sub_items(self):
        return self.items

    @property
    def useful_items(self):
        return self.items

    def __repr__(self):
        s = '<%s[' % self.type
        for item in self.items:
            s += repr(item)
            s += ', '
        s += ']>'
        return s

    def get_definitions(self):
        """
        找到内部的声明
        """
        mis = self.get_meta_infos()
        if not mis.has_key('definitions'):
            mis['definitions'] = dict()
        return mis['definitions']

    def add_definition(self, name):
        """
        TODO: 添加声明，暂时只记录名称，不记录具体类型等元信息
        """
        definitions = self.get_definitions()
        definitions[name] = None

    def add_definitions(self, names):
        """
        TODO: 批量添加声明
        """
        definitions = self.get_definitions()
        for name in names:
            definitions[name] = None

    def get_definition_names(self):
        """
        找到内部的声明的名称列表
        """
        return self.get_definitions().keys()

    def find_definitions(self):
        if self.has_got_definitions:
            return
        self.has_got_definitions = True
        # print("finding definitions")
        for item in self.useful_items:
            if isinstance(item, JsExpression):
                item.find_definitions()
                definitions = item.get_definition_names()
                self.add_definitions(definitions)
        # print('find definitions done')
        # print(self.get_definition_names())


class JsCommonExpression(JsExpression):
    """
    除了其他JsExpression之类之外所有的JsExpression实例都使用这个类，避免JsExpression直接被构造
    """

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # print('common expr')
        context.add_to_derive_items(self.useful_items)


class JsStatement(JsExpression):
    type = 'statement'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsStatement.type)

    def analyse(self, context):
        """
        statement的推导：
        将语句内容放入推导队列中，
        合并现有语句内容的元信息，得到自身的元信息
        """
        JsExpression.analyse(self, context)
        # print('analysing statement')
        context.add_to_derive_items(self.useful_items)


class JsReturnStatement(JsStatement):
    type = 'return_statement'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsReturnStatement.type)

    def analyse(self, context):
        """
        TODO: 返回语句的推导，给所在的func_body的return的元信息增加类型信息，增加的类型信息来源于return语句的内容
        """
        JsExpression.analyse(self, context)


class JsProgram(JsExpression):
    type = 'program'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsProgram.type)
        self.is_lexical_scope = True

    def analyse(self, context):
        JsExpression.analyse(self, context)
        if self.analysed_count > 1:
            # program 只能被推导1次,TODO: 应该改成推导到足够的信息为止
            return
        else:
            """
            TODO: 合并内容的元信息，得到自身的元信息
            """
            context.add_to_derive_items(self.items)


class JsStmtBlock(JsExpression):
    type = 'stmt_block'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsStmtBlock.type)
        self.is_lexical_scope = True

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # print('analysing stmt block')
        self.find_definitions()
        """
        TODO: 合并内容的元信息，得到自身的元信息
        """
        JsExpression.lexical_scope_analyse(self, context)


class JsFinalStatement(JsStatement):
    # expression ended with ;
    type = 'final_statement'

    def analyse(self, context):
        JsExpression.analyse(self, context)


class JsSymbol(GObject):
    type = 'symbol'


class JsNewLine(GObject):
    type = 'newline'

    def __repr__(self):
        return "<%s[%s\\n]>" % (self.type, len(self.value))

    instances = []

    @staticmethod
    def add_instance(newline):
        JsNewLine.instances.append(newline)

    @staticmethod
    def init():
        JsNewLine.instances = []


class JsObject(JsExpression):
    type = 'object'


# such as a, b, c
class JsExpressionList(JsExpression):
    type = 'expression_list'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsExpressionList.type)

    def get_objects(self):
        pass  # TODO: get objects in the list, eg. a and b and c are objects in 'a, b, c'

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: 搜集各元素的元信息，并得到自身的元信息


class JsBlock(JsExpression):
    type = 'block'
    items = []

    def __init__(self, items):
        JsExpression.__init__(self, items, JsBlock.type)

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # print('analysing block')
        # 代码块，将内容加入推导队列即可
        self.lexical_scope_analyse(context)


class JsAssignExpression(JsExpression):
    type = 'assign_expression'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsAssignExpression.type)
        self.left = items[0]
        self.op = items[1]
        self.right = items[2]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        """
        TODO: 赋值表达式，很重要，可以得到重要的类型信息
        """


class JsVarExpression(JsExpression):
    type = 'var_expression'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsVarExpression.type)
        # TODO: split the declare variable info
        # print(items)

    def analyse(self, context):
        # TODO: 目前没有执行到这里
        JsExpression.analyse(self, context)
        """
        TODO: 声明表达式，很重要，可以得到重要的类型信息，要注意var表达式的内容可能有很多种
        """
        if self.items[1].type == JsVarItemList:
            # VAR var_item_list
            # TODO: 把变量什么加入当前词法作用域中
            var_items = self.items[1].useful_items
            # print('var item list:', var_items)
        elif self.items[1].type == JsExpressionList:
            # TODO
            # print('var expression_list', self.items[1])
            pass


class JsVarItemList(JsExpression):
    type = 'var_item_list'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsVarItemList.type)

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO

    @property
    def useful_items(self):
        return filter(lambda x: isinstance(x, JsIdentity) or isinstance(x, JsAssignExpression),
                      self.items)

    def find_definitions(self):
        if self.has_got_definitions:
            return
        self.has_got_definitions = True
        items = self.useful_items
        for item in items:
            if isinstance(item, JsIdentity):
                self.add_definition(item.value)
            elif isinstance(item, JsAssignExpression):
                if isinstance(item.useful_items[0], JsIdentity):
                    self.add_definition(item.useful_items[0].value)


class JsIdentifierList(JsExpression):
    type = 'identifier_list'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsIdentifierList.type)
        # TODO: split the identifiers and commas when fetch them

    def get_identifiers(self):
        return filter(lambda x: isinstance(x, JsIdentity), self.items)

    def analyse(self, context):
        JsExpression.analyse(self, context)
        """
        TODO
        """
        # print("iden list: ", self.get_identifiers())


class JsJsonArray(JsExpression):
    type = 'json_array'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsJsonArray.type)
        # TODO: split the items from '[', ']', and ','

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: 获取自身类型信息


class JsJsonObject(JsExpression):
    type = 'json_object'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsJsonObject.type)
        # TODO: split the '{', '}', ',', and the items splited by ':'

    def reformat(self):
        """
        重构这个对象的结构，不要保留 json_object_item_list 对象
        """
        pass

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: 获取自身类型信息


class JsJsonObjectItemList(JsExpression):
    type = 'json_object_item_list'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsJsonObjectItemList.type)

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: 合并各json object item的元信息，即是各个item的property信息


class JsJsonObjectItem(JsExpression):
    type = 'json_object_item'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsJsonObjectItem.type)
        # TODO: split the colon ':'

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: 表示这一个JSON Object item有一个属性property的Name，这个Name对应值有类型等元信息


class JsTypeOfExpression(JsExpression):
    type = 'typeof_expression'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsTypeOfExpression.type)
        self.typeof_symbol = items[0]
        self.real_expression = items[1]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: 返回值是字符串类型


class JsXkhExpression(JsExpression):
    """
    eg. '( expression )' is also expression
    """
    type = 'xkh_expression'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsXkhExpression.type)
        self.real_expression = items[1]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # 元信息就是内容的元信息，保持不变
        self.meta_proxy = self.real_expression
        context.add_to_derive_items([self.real_expression])


class JsArrayGetValueExpression(JsExpression):
    type = 'array_get_value_expression'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsArrayGetValueExpression.type)
        # judge the type is a[b] or a.b
        if len(items) < 4:
            self.get_value_type = 'point'  # a.b
        else:
            self.get_value_type = 'array'  # a[b]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: self的元信息根据 a[b]中的a的类型信息判断，如果a是[A]类型的，则self是A类型的
        context.add_to_derive_items(self.items)


class JsVoidExpression(JsExpression):
    type = 'void'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsVoidExpression.type)

    def analyse(self, context):
        JsExpression.analyse(self, context)
        self.add_type_meta_info(types.GTypes.VOID)
        # print('type void')
        context.add_to_derive_items(self.items)


class JsForExpression(JsExpression):
    type = 'for'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsForExpression.type)
        self.is_lexical_scope = True

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: for 循环
        context.add_to_derive_items(self.items)


class JsIfExpression(JsExpression):
    type = 'if'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsIfExpression.type)
        # TODO split 'if', 'else', if-clause and else-clause

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: if 语句，要按if, if-else判断返回类型
        context.add_to_derive_items(self.items)


class JsInstanceOfExpression(JsExpression):
    type = 'instanceof'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsInstanceOfExpression.type)
        self.left = items[0]
        self.op = items[1]
        self.right = items[2]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        self.add_type_meta_info(types.GTypes.BOOL)
        context.add_to_derive_items([self.left, self.right])


class JsWhileExpression(JsExpression):
    type = 'while'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsWhileExpression.type)
        self.while_symbol = items[0]
        self.while_condition = items[2]
        self.while_body = items[4]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        context.add_to_derive_items(self.items)


class JsDoWhileExpression(JsExpression):
    type = 'do-while'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsDoWhileExpression.type)
        self.do_symbol = items[0]
        self.do_while_body = items[1]
        self.while_symbol = items[2]
        self.while_condition = items[4]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        context.add_to_derive_items(self.items)


class JsFuncCall(JsExpression):
    type = 'func_call'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsFuncCall.type)
        self.func = items[0]
        if len(items) == 3:
            self.func_call_arguments = None  # TODO: use an empty expression-list
        else:
            self.func_call_arguments = items[2]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: 函数调用，返回类型按函数的返回类型算，同时函数的返回类型可以用来推导参数类型，函数调用本身所在上下文的信息等，重要！
        # print('func call here, TODO')
        context.add_to_derive_items(self.items)
        context.add_to_derive_items([self])


class JsFuncDef(JsExpression):
    type = 'func_def'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsFuncDef.type)
        if isinstance(items[1], JsIdentity):
            self.func_name = items[1]
        else:
            self.func_name = None
        if isinstance(items[-3], JsFuncParamsList):
            self.func_params = items[-3]
        else:
            self.func_params = JsFuncParamsList([])
        self.func_body = items[-1]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: 函数定义，非常重要
        # 如果是非匿名函数，则把这个函数加入所在词法作用域中
        if self.func_name is not None:
            scope = self.get_parent_lexical_scope(context)
            if scope is not None:
                scope.add_scope_env_item(self.func_name, self)
        # print('func(%s) def here, TODO' % self.func_name)
        context.add_to_derive_items(self.items)
        context.add_to_derive_items([self])


class JsFuncBody(JsExpression):
    type = 'func_body'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsFuncBody.type)
        self.is_lexical_scope = True
        if isinstance(items[0], JsSymbol) and items[0].value == '{':
            self.func_body_type = 'dkh'
            self.body = items[1]
        elif isinstance(items[0], JsJsonObject):
            self.func_body_type = 'json_object'
            self.body = items[0]
        else:
            self.func_body_type = 'expression'
            self.body = items[0]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: 函数定义的函数体，非常重要
        context.add_to_derive_items(self.items)
        context.add_to_derive_items([self])


class JsFuncParamsList(JsExpression):
    type = 'func_params_list'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsFuncParamsList.type)

    def get_identifiers(self):
        pass  # TODO: get the items after add all

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: 函数定义的参数列表，非常重要
        context.add_to_derive_items(self.items)
        context.add_to_derive_items([self])


class JsSwitchExpression(JsExpression):
    type = 'switch'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsSwitchExpression.type)
        self.switch_condition = items[2]
        self.switch_body = items[-1]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        context.add_to_derive_items(self.items)


class JsSwitchBody(JsExpression):
    type = 'switch_body'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsSwitchBody.type)
        self.body = items[1]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        context.add_to_derive_items([self.body])


class JsSwitchCase(JsExpression):
    type = 'switch_case'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsSwitchCase.type)
        if items[0].value == 'case':
            self.switch_case_type = 'case'
        elif items[0].value == 'default':
            self.switch_case_type = 'default'
        else:
            self.switch_case_type = None  # unknown error
            # TODO

    def analyse(self, context):
        JsExpression.analyse(self, context)
        context.add_to_derive_items(self.items)


class JsSwitchCaseEnd(JsExpression):
    type = 'switch_case_end'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsSwitchCaseEnd.type)
        # TODO

    def analyse(self, context):
        JsExpression.analyse(self, context)
        context.add_to_derive_items(self.items)


class JsSwitchCaseList(JsExpression):
    type = 'switch_case_list'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsSwitchCaseList.type)

    def analyse(self, context):
        JsExpression.analyse(self, context)
        context.add_to_derive_items(self.items)


class JsQuestionExpression(JsExpression):
    type = 'question'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsQuestionExpression.type)
        self.question_expression = items[0]
        self.true_expression = items[2]
        self.false_expression = items[4]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        self.question_expression.add_type_meta_info(types.GTypes.OPTIONAL_BOOL)
        self.add_type_meta_info(types.GOptionalType([self.true_expression.get_type_meta_info(),
                                                     self.false_expression.get_type_meta_info()]))
        context.add_to_derive_items(self.items)
        context.add_to_derive_items([self])


class JsBlockWithDkh(JsExpression):
    type = 'block_with_dkh'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsBlockWithDkh.type)
        self.body = items[1]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        context.add_to_derive_items([self.body])


class JsInExpression(JsExpression):
    type = 'in'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsInExpression.type)
        self.sub_expression = items[0]
        self.from_expression = items[2]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: 如果self.from_expression是[A]类型，则self.sub_expression是A类型，表达式本身是void类型
        self.add_type_meta_info(types.GTypes.VOID)
        from_expression_type = self.from_expression.get_type_meta_info()
        if from_expression_type is not None and isinstance(from_expression_type, types.GArrayType):
            self.sub_expression.add_type_meta_info(from_expression_type.type)
        context.add_to_derive_items([self.sub_expression, self.from_expression])


class JsForHead(JsExpression):
    type = 'for_head'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsForHead.type)
        # TODO

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: for head
        context.add_to_derive_items(self.items)


class JsTry(JsExpression):
    type = 'try'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsTry.type)
        # TODO

    def analyse(self, context):
        JsExpression.analyse(self, context)
        context.add_to_derive_items(self.items)


class JsCatch(JsExpression):
    type = 'catch'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsCatch.type)
        # TODO

    def analyse(self, context):
        JsExpression.analyse(self, context)
        context.add_to_derive_items(self.items)


class JsNewExpression(JsExpression):
    type = 'new'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsNewExpression.type)
        self.new_type_body = items[1]

    def analyse(self, context):
        JsExpression.analyse(self, context)
        # TODO: new表达式，重要信息，返回类型信息是new 后表达式的值信息，
        # TODO: 比如如果后面是一个函数，则new表达式的返回类型是这个函数的名称（如果不是匿名函数的话），且记录下这个函数的ID
        context.add_to_derive_items([self.new_type_body])


class JsDeleteExpression(JsExpression):
    type = 'delete'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsDeleteExpression.type)

    def analyse(self, context):
        JsExpression.analyse(self, context)
        context.add_to_derive_items(self.items)


class JsThrowExpression(JsExpression):
    type = 'throw'

    def __init__(self, items):
        JsExpression.__init__(self, items, JsThrowExpression.type)

    def analyse(self, context):
        JsExpression.analyse(self, context)
        context.add_to_derive_items(self.items)


def init_lex_yacc():
    JsComment.init()
    JsNewLine.init()