# coding: UTF-8
from __future__ import print_function

tokens = (
    'COMMENT', 'STRING', 'BOOLEAN', 'NUMBER', 'REGEX',
    'VAR', 'FUNCTION', 'RETURN', 'FOR', 'WHILE', 'SWITCH', 'CASE', 'BREAK', 'CONTINUE', 'IF', 'ELSE', 'THROW', 'DO',
    'NEW', 'DELETE', 'TRY', 'CATCH', 'FINALLY', 'DEFAULT', 'INSTANCEOF', 'TYPEOF', 'VOID', 'IN',
    'LEFT_XKH', 'RIGHT_XKH',
    'LEFT_ZKH', 'RIGHT_ZKH',
    'LEFT_DKH', 'RIGHT_DKH',
    'SEMICOLON', 'POINT', 'COMMA', 'COLON', 'QUESTION',
    'RIGHT_OPERATOR', # right operator is like !aVar
    'LEFT_RIGHT_OPERATOR', # --, ++
    'ASSIGN', 'BINARY_OPERATOR',
    'IDENTIFIER',
    'NEWLINE',
)

import js_types as types

types.init_lex_yacc()


def t_POINT(t):
    r'\.'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_COMMA(t):
    r','
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_COLON(t):
    r':'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_RIGHT_XKH(t):
    r'\)'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_LEFT_ZKH(t):
    r'\['
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_RIGHT_ZKH(t):
    r'\]'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_LEFT_DKH(t):
    r'\{'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_RIGHT_DKH(t):
    r'}'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_THROW(t):
    r'throw(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_NEW(t):
    r'new(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_IN(t):
    r'in(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_DELETE(t):
    r'delete(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_VAR(t):
    r'var(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_VOID(t):
    r'void(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_FUNCTION(t):
    r'function(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_RETURN(t):
    r'return(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_TYPEOF(t):
    r'typeof(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_FOR(t):
    r'for(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_IF(t):
    r'if(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_INSTANCEOF(t):
    r'instanceof(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_DO(t):
    r'do(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_LEFT_RIGHT_OPERATOR(t):
    r'(--)|(\+\+)'
    t.value = types.JsOperator(t.value, t.lineno, t.lexpos)
    return t


def t_DEFAULT(t):
    r'default(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_ELSE(t):
    r'else(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_TRY(t):
    r'try(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_CATCH(t):
    r'catch(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_FINALLY(t):
    r'finally(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_WHILE(t):
    r'while(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_SWITCH(t):
    r'switch(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_CASE(t):
    r'case(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_BREAK(t):
    r'break(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_CONTINUE(t):
    r'continue(?![\w\$])'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_COMMENT(t):
    r'(//[^\n]*)|(/\*(\s|.)*?\*/)'
    t.lexer.lineno += t.value.count('\n')
    t.value = types.JsComment(t.value, t.lineno, t.lexpos)
    types.JsComment.add_instance(t.value)
    # return t


def t_LEFT_XKH(t):
    r'\('
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_SEMICOLON(t):
    r';'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_ASSIGN(t):
    r'(?<![=\+\-\*/])=(?!=)'
    t.value = types.JsAssignOperator(t.value, t.lineno, t.lexpos)
    return t


def t_REGEX(t):
    r'/(?!\s)(([^/\n])|(\/))+?(((?<![\\\s])/)|((?<![\s])(\\\\/)))[\w]*'
    t.value = types.JsRegex(t.value, t.lineno, t.lexpos)
    return t


def t_QUESTION(t):
    r'\?'
    t.value = types.JsRegex(t.value, t.lineno, t.lexpos)
    return t


def t_RIGHT_OPERATOR(t):
    r'!(?!=)|(\+\+)|(--)|(~)'
    t.value = types.JsSymbol(t.value, t.lineno, t.lexpos)
    return t


def t_BINARY_OPERATOR(t):
    r'[\*/|%^]|(>(?!=))|(<(?!=))|(\+(?![\+=]))|(-(?![-=]))|(&&)|(\|\|)|(&)|(\|)|(!==)|(==(?!=))|(===)|(!=)|(\+=)|(-=)|(\*=)|(>=)|(<=)|(\|=)|(&=)|(/=)'
    t.value = types.JsCalcOperator(t.value, t.lineno, t.lexpos)
    return t


def t_STRING(t):
    r"(?:\"((?:\\.|[^\"])*)\"|'((?:\\.|[^'])*)')"
    t.value = types.JsString(t.value, t.lineno, t.lexpos)
    return t


def t_BOOLEAN(t):
    r'(true)|(false)'
    if t.value == 'true':
        t.value = True
    else:
        t.value = False
    t.value = types.JsBoolean(t.value, t.lineno, t.lexpos)
    return t


def t_NUMBER(t):
    r'[+-]?(?:(0x[0-9a-fA-F]+|0[0-7]+)|((?:[0-9]+(?:\.[0-9]*)?|\.[0-9]+)(?:[eE][+-]?[0-9]+)?|NaN|Infinity))'
    source_text = t.value
    t.value = types.JsNumber(source_text, t.value, t.lineno, t.lexpos)
    return t


def t_IDENTIFIER(t):
    r'[a-zA-Z_\$]*[\w\$]+'
    if t.value in types.JsKeyword.keywords:
        t.value = types.JsKeyword(t.value, t.lineno, t.lexpos)
    else:
        t.value = types.JsIdentity(t.value, t.lineno, t.lexpos)
    return t


def t_NEWLINE(t):
    r'(\r\n|\n)+'
    t.lexer.lineno += t.value.count('\n')
    t.value = types.JsNewLine(t.value, t.lineno, t.lexpos)
    types.JsNewLine.add_instance(t.value)
    # return t


def t_SPACE(t):
    r'[\t ]+'
    #t.value = ' '
    #return t


def t_error(t):
    print("Illegal character '%s' at %d, %d" % (t.value[:10], t.lineno, t.lexpos))
    t.lexer.skip(1)


import ply.lex as lex


identifiers = {}


def p_program(p):
    """
    program : stmt_block program
            | stmt_block
    """
    if len(p) == 2:
        p[0] = types.JsProgram([p[1]])
    else:
        p[0] = p[2]
        p[0].add(p[1])


def p_stmt_block(p):
    """
    stmt_block : statement stmt_block
                | statement
                | error
    """
    p[0] = types.JsStmtBlock(p[1:])


def p_return_statement(p):
    """
    return_statement : RETURN statement
                    | RETURN
    """
    p[0] = types.JsReturnStatement(p[1:])


def p_statement_not_block(p):
    """
    statement_not_block : expression SEMICOLON
            | expression_list SEMICOLON
            | expression
            | expression_list
            | SEMICOLON
            | return_statement
    """
    if isinstance(p[1], types.JsStatement):
        p[0] = p[1]  # return_statement
    else:
        p[0] = types.JsStatement(p[1:])


def p_statement(p):
    """
    statement : statement_not_block
            | block_with_dkh
    """
    if isinstance(p[1], types.JsStatement):
        p[0] = p[1]
    else:
        p[0] = types.JsStatement(p[1:])


def p_identifier_assign_expression(p):
    """
    identifier_assign_expression : IDENTIFIER ASSIGN expression
    """
    p[0] = types.JsAssignExpression(p[1:])


def p_assign_expression(p):
    """
    assign_expression : identifier_assign_expression
                    | array_get_value_expression ASSIGN expression
    """
    if isinstance(p[1], types.JsAssignExpression):
        p[0] = p[1]
    else:
        p[0] = types.JsAssignExpression(p[1:])


def p_var_expression(p):
    """
    var_expression : VAR expression_list
             | VAR var_item_list
    """
    p[0] = types.JsVarExpression(p[1:])


def p_var_item_list(p):
    """
    var_item_list : var_item COMMA var_item_list
                | var_item
    """
    if len(p) == 2:
        p[0] = types.JsVarItemList([p[1]])
    else:
        p[0] = p[3]
        p[0].insert(0, p[2])
        p[0].insert(0, p[1])


def p_var_item(p):
    """
    var_item : identifier_assign_expression
            | IDENTIFIER
    """
    p[0] = p[1]


def p_identifier_list(p):
    """
    identifier_list : IDENTIFIER COMMA identifier_list
                | IDENTIFIER
    """
    if len(p) == 2:
        p[0] = types.JsIdentifierList(p[1:])
    else:
        p[0] = p[3]
        p[0].insert(0, p[2])
        p[0].insert(0, p[1])


def p_json_array(p):
    """
    json_array : LEFT_ZKH expression_list RIGHT_ZKH
                | LEFT_ZKH expression_list COMMA RIGHT_ZKH
                | LEFT_ZKH RIGHT_ZKH
    """
    p[0] = types.JsJsonArray(p[1:])


def p_xkh_expression(p):
    """
    xkh_expression : LEFT_XKH expression RIGHT_XKH
    """
    p[0] = types.JsXkhExpression(p[1:])


def p_expression(p):
    """
    expression : var_expression
            | assign_expression
            | const_expression
            | IDENTIFIER
            | xkh_expression
            | func_call
            | func_declare
            | for_expression
            | expression BINARY_OPERATOR expression
            | RIGHT_OPERATOR expression
            | BINARY_OPERATOR expression
            | LEFT_RIGHT_OPERATOR expression
            | LEFT_RIGHT_OPERATOR expression newline
            | array_get_value_expression
            | array_get_value_expression newline
            | switch_expression
            | while_expression
            | do_while_expression
            | try_expression
            | new_expression
            | delete_expression
            | throw_expression
            | BREAK
            | CONTINUE
            | if_expression
            | instanceof_expression
            | typeof_expression
            | void_expression
            | in_expression
            | json_object
            | json_array
            | empty_json_object
            | expression LEFT_RIGHT_OPERATOR
            | expression LEFT_RIGHT_OPERATOR newline
            | question_expression
            | LEFT_XKH expression_list RIGHT_XKH
    """
    p[0] = types.JsCommonExpression(p[1:], 'expression')


def p_array_get_value_expression(p):
    """
    array_get_value_expression : expression LEFT_ZKH expression RIGHT_ZKH
                    | expression POINT expression
    """
    p[0] = types.JsArrayGetValueExpression(p[1:])


def p_empty_json_object(p):
    """
    empty_json_object : LEFT_DKH RIGHT_DKH
    """
    p[0] = types.JsJsonObject(p[1:])


def p_json_object(p):
    """
    json_object : LEFT_DKH json_object_item_list RIGHT_DKH
            | LEFT_DKH json_object_item_list COMMA RIGHT_DKH
            | empty_json_object
    """
    if len(p) < 3:
        p[0] = p[1]
    else:
        p[0] = types.JsJsonObject(p[1:])


def p_json_object_item_list(p):
    """
    json_object_item_list : json_object_item COMMA json_object_item_list
                        | json_object_item
    """
    p[0] = types.JsJsonObjectItemList(p[1:])


def p_json_object_item(p):
    """
    json_object_item : json_object_key COLON expression
    """
    p[0] = types.JsJsonObjectItem(p[1:])


def p_json_object_key(p):
    """
    json_object_key : STRING
                    | IDENTIFIER
                    | NUMBER
    """
    p[0] = p[1]


def p_typeof_expression(p):
    """
    typeof_expression : TYPEOF expression
    """
    p[0] = types.JsTypeOfExpression(p[1:])


def p_for_expression(p):
    """
    for_expression : FOR for_head block
    """
    p[0] = types.JsForExpression(p[1:])


def p_void_expression(p):
    """
    void_expression : VOID expression
    """
    p[0] = types.JsVoidExpression(p[1:])


def p_if_condition(p):
    """
    if_condition : expression
    """
    p[0] = p[1]


def p_if_expression(p):
    """
    if_expression : IF if_condition block ELSE block
                | IF if_condition block
    """
    p[0] = types.JsIfExpression(p[1:])


def p_instanceof_expression(p):
    """
    instanceof_expression : expression INSTANCEOF expression
    """
    p[0] = types.JsInstanceOfExpression(p[1:])


def p_while_expression(p):
    """
    while_expression : WHILE LEFT_XKH expression RIGHT_XKH block
    """
    p[0] = types.JsWhileExpression(p[1:])


def p_do_while_expression(p):
    """
    do_while_expression : DO block WHILE LEFT_XKH expression RIGHT_XKH
    """
    p[0] = types.JsDoWhileExpression(p[1:])


def p_func_call(p):
    """
    func_call : expression LEFT_XKH RIGHT_XKH
            | expression LEFT_XKH expression_list RIGHT_XKH
    """
    p[0] = types.JsFuncCall(p[1:])


def p_func_declare(p):
    """
    func_declare : FUNCTION IDENTIFIER LEFT_XKH RIGHT_XKH func_body
                | FUNCTION IDENTIFIER LEFT_XKH func_params_list RIGHT_XKH func_body
                | FUNCTION LEFT_XKH RIGHT_XKH func_body
                | FUNCTION LEFT_XKH func_params_list RIGHT_XKH func_body
    """
    p[0] = types.JsFuncDef(p[1:])


def p_func_body(p):
    """
    func_body : empty_json_object
            | LEFT_DKH stmt_block RIGHT_DKH
            | json_object
            | expression
    """
    p[0] = types.JsFuncBody(p[1:])


def p_func_params_list(p):
    """
    func_params_list : IDENTIFIER COMMA func_params_list
                    | IDENTIFIER
    """
    if len(p) <= 2:
        p[0] = types.JsFuncParamsList(p[1:])
    else:
        p[0] = p[3]
        p[0].insert(0, p[2])
        p[0].insert(0, p[1])


def p_switch_expression(p):
    """
    switch_expression : SWITCH LEFT_XKH expression RIGHT_XKH switch_body
    """
    p[0] = types.JsSwitchExpression(p[1:])


def p_switch_body(p):
    """
    switch_body : LEFT_DKH switch_case_list RIGHT_DKH
    """
    p[0] = types.JsSwitchBody(p[1:])


def p_question_expression(p):
    """
    question_expression : expression QUESTION expression COLON expression
    """
    p[0] = types.JsQuestionExpression(p[1:])


def p_switch_case(p):
    """
    switch_case : CASE expression COLON block switch_case_end
                | CASE expression COLON block
                | CASE expression COLON statement switch_case_end
                | CASE expression COLON statement
                | CASE expression COLON
                | DEFAULT COLON block
                | DEFAULT COLON statement
                | DEFAULT COLON block switch_case_end
                | DEFAULT COLON statement switch_case_end
    """
    p[0] = types.JsSwitchCase(p[1:])


def p_switch_case_end(p):
    """
    switch_case_end : BREAK
                | BREAK SEMICOLON
                | RETURN
                | RETURN SEMICOLON
                | SEMICOLON switch_case_end
    """
    p[0] = types.JsSwitchCaseEnd(p[1:])


def p_switch_case_list(p):
    """
    switch_case_list : switch_case switch_case_list
                    | switch_case
    """
    if len(p) <= 2:
        p[0] = types.JsSwitchCaseList(p[1:])
    else:
        p[0] = p[2]
        p[0].insert(0, p[1])


def p_const_expression(p):
    """
    const_expression : NUMBER
                 | STRING
                 | BOOLEAN
                 | REGEX
    """
    p[0] = p[1]


def p_newline(p):
    """newline : NEWLINE newline
                | COMMENT newline
                | NEWLINE
                | COMMENT
    """
    p[0] = p[1]


def p_expression_list(p):
    """
    expression_list : expression COMMA expression_list
                | expression COMMA
                | expression
    """
    if len(p) == 4:
        p[0] = p[3]
        p[0].insert(0, p[1])
        p[0].insert(1, p[2])
    elif len(p) == 3:
        p[0] = types.JsExpressionList(p[1:])
    else:
        p[0] = types.JsExpressionList(p[1:])


def p_block_with_dkh(p):
    """
    block_with_dkh : LEFT_DKH stmt_block RIGHT_DKH
        | empty_json_object
    """
    if len(p) <= 2:
        p[0] = p[1]
    else:
        p[0] = types.JsBlockWithDkh(p[1:])


def p_block(p):
    """
    block : block_with_dkh
        | statement_not_block
    """
    p[0] = p[1]


def p_for_head(p):
    """
    for_head : LEFT_XKH stmt_block RIGHT_XKH
        | LEFT_XKH statement statement RIGHT_XKH
        | LEFT_XKH statement statement expression RIGHT_XKH
        | LEFT_XKH in_expression RIGHT_XKH
        | LEFT_XKH VAR in_expression RIGHT_XKH
    """
    p[0] = types.JsForHead(p[1:])


def p_in_expression(p):
    """
    in_expression : expression IN expression
    """
    p[0] = types.JsInExpression(p[1:])


def p_try_expression(p):
    """
    try_expression : TRY block catch_head block FINALLY block
        | TRY block catch_head block
        | TRY block FINALLY block
        | TRY block
    """
    p[0] = types.JsTry(p[1:])


def p_catch_head(p):
    """
    catch_head : CATCH LEFT_XKH IDENTIFIER RIGHT_XKH
    """
    p[0] = types.JsCatch(p[1:])


def p_new_expression(p):
    """
    new_expression : NEW expression
    """
    p[0] = types.JsNewExpression(p[1:])


def p_delete_expression(p):
    """
    delete_expression : DELETE expression
                | DELETE expression SEMICOLON
    """
    p[0] = types.JsDeleteExpression(p[1:])


def p_throw_expression(p):
    """
    throw_expression : THROW expression
    """
    p[0] = types.JsThrowExpression(p[1:])

#
# def p_break_expression(p):
#     """
#     break_expression : BREAK
#     """
#     p[0] = types.JsCommonExpression(p[1:], 'break_expression')
#
#
# def p_continue_expression(p):
#     """
#     continue_expression : CONTINUE
#     """
#     p[0] = types.JsCommonExpression(p[1:], 'continue_expression')


def p_error(p):
    if p:
        print("Syntax error at '%s'" % p.value)
        print(p.lineno, p.lexpos, p.type)
    else:
        print("Syntax error at EOF")
        ## Read ahead looking for a closing '}'
        #while 1:
        #    tok = yacc.token()             # Get the next token
        #    if not tok or tok.type == 'RBRACE': break
        #yacc.restart()


import ply.yacc as yacc


def run_lex(text):
    lex.lex()
    lex.input(text)
    while True:
        token = lex.token()
        if not token:
            break
        print(token)


def run_yacc(text):
    text = text + '\n'
    lex.lex()
    yacc.yacc()
    result = yacc.parse(text)
    return result

