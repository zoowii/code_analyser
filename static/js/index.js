$(function () {
    var wsUrl = 'ws://' + window.location.host + '/socket';
    var ws = new WebSocket(wsUrl);
    ws.onopen = function () {
        console.log('websocket opened');
    };
    ws.onclose = function () {
        console.log('websocket closed');
    };
    ws.onmessage = function (evt) {
        console.log("on msg: ", evt);
        onWebsocketMessage(evt.data);
    };
    ws.onerror = function (evt) {
        console.log('websocket error: ', evt);
    };
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/monokai");
    editor.getSession().setMode("ace/mode/javascript");

    function onWebsocketMessage(msg) {
        var json = JSON.parse(msg);
        console.log(json);
        switch (json.event) {
            case 'code_parsed':
            {
                $("#display-panel").html(JSON.stringify(json.data));
            }
                break;
        }
    }

    function randomString(length) {
        var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');

        if (!length) {
            length = Math.floor(Math.random() * chars.length);
        }

        var str = '';
        for (var i = 0; i < length; i++) {
            str += chars[Math.floor(Math.random() * chars.length)];
        }
        return str;
    }

    function sendCodeToParse(code) {
        ws.send(JSON.stringify({
            data: code,
            event: 'send_code_to_parse',
            id: randomString(20)
        }))
    }

    $(".send-file-to-parse-btn").click(function () {
        var code = editor.getValue();
        sendCodeToParse(code);
    });

    $(".file-list li").dblclick(function () {
        var filename = $(this).html();
        $.get('/webide/load_file', {
            filename: filename
        }, function (data) {
            editor.setValue(data);
            editor.gotoLine(1, 1, false);
            sendCodeToParse(data);
        });
    });
});