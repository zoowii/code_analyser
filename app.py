# coding: UTF-8
from __future__ import print_function
from flask import Flask

app = Flask(__name__)


@app.route('/')
def index():
    return 'hello'


if __name__ == '__main__':
    port = 5000
    app.run(debug=True, port=port, host='0.0.0.0')