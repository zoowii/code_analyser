# coding: UTF-8
from __future__ import print_function
import os
import util.file as file_util
from pprint import pprint

cur_dir = os.path.dirname(__file__)

code0 = file_util.readfile(os.path.join(cur_dir, 'test_codes', 'tmp_test.js'))
code1 = file_util.readfile(os.path.join(cur_dir, 'test_codes', 'underscore.js'))
code2 = file_util.readfile(os.path.join(cur_dir, 'test_codes', 'backbone.js'))
code3 = file_util.readfile(os.path.join(cur_dir, 'test_codes', 'jquery2.js'))
code4 = file_util.readfile(os.path.join(cur_dir, 'test_codes', 'jquery2.min.js'))
from parser import js2
from parser.js2 import run_yacc
from core.js_analyser import JsContext

result = run_yacc(code0)
pprint(repr(result)[:100])
result = run_yacc(code1)
pprint(repr(result)[:100])
result = run_yacc(code2)
pprint(repr(result)[:100])
# result = run_yacc(code3)
# pprint(repr(result)[:100])

# result = js2.run_yacc(code4)
# pprint(repr(result)[:100])

print("now is analysing underscore.js")
underscore_program = run_yacc(code1)

context = JsContext()
context.analyse_program(underscore_program, 'underscore.js')