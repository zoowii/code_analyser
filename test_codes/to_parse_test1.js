var fs = require('fs');
var codeBuf = fs.readFileSync(__filename);
codeBuf.encoding = 'utf8';
var code = codeBuf.toString();
var stream = parser.parseString(code);
var Condition = parser.Condition,
    OrCondition = parser.OrCondition,
    AndCondition = parser.AndCondition,
    NotCondition = parser.NotCondition,
    RegexCondition = parser.RegexCondition,
    EmptyCondition = parser.EmptyCondition,
    RangeCondition = parser.RangeCondition,
    FixedCondition = parser.FixedCondition
    , OperationCondition = parser.OperationCondition
    , Element = parser.Element
    , CharElement = parser.CharElement;
//stream.addCondition(singleStringCond);
stream.addCondition(lineCommentCond);
stream.addCondition(blockCommentCond);
var regexEmptyCond = new RegexCondition("\\s+", 'empty_spaces');

var a = "hello " + "world";
