# coding: UTF-8
from __future__ import print_function
from gevent import pywsgi
from geventwebsocket.handler import WebSocketHandler
from webide.application import app
import os

if __name__ == '__main__':
    # app.run(host='127.0.0.1', port=5000, debug=True)
    host = '0.0.0.0'
    port = int(os.environ.get('PORT', '5000'))
    server = pywsgi.WSGIServer((host, port), app, handler_class=WebSocketHandler)
    print('serving at %s:%d' % (host, port))
    server.serve_forever()
