# coding: UTF-8
from __future__ import print_function
import uuid


class GObjectMetaInfo(object):
    """
    语法元素的元信息
    """

    def __init__(self, name, value):
        self.name = name
        self.value = value
        self.analysed_count = 1  # 此元信息在此元素上被推导出来的次数

    @property
    def reliability(self):
        """
        可靠的，暂时用元信息被推导出来的次数计算
        """
        return self.analysed_count


class GType(object):
    """
    类型信息，表示元素的类型
    """
    UNKNOWN = None  # 未知类型，无法推导的类型

    def __init__(self, type, is_fixed=False):
        self.type = type
        self.is_fixed = is_fixed


class GTypes(object):
    """
    基本的类型信息
    """
    VOID = GType('void', True)
    OPTIONAL_VOID = GType('void')
    BOOL = GType('bool', True)
    OPTIONAL_BOOL = GType('bool')
    NUMBER = GType('number', True)
    OPTIONAL_NUMBER = GType('number')
    STRING = GType('string', True)
    OPTIONAL_STRING = GType('string')
    REGEX = GType('regex', True)
    OPTIONAL_REGEX = GType('regex')


class GOptionalType(GType):
    """
    可选类型的类型信息，在一个范围内中的某个类型
    """

    def __init__(self, types):
        self.types = types
        if len(types) > 1 and isinstance(types[0], GOptionalType):
            self.types = types[0].types
            self.types.extend(types[1:])
            # TODO: 如果其中有一样的类型，合并，如果其中也有Optional类型，合并，去除空值


class GArrayType(GType):
    """
    数组类型的类型信息
    """

    def __init__(self, type):
        self.type = type


# TODO: GJsonType


class GObject(object):
    type = 'object'
    value = None

    def __init__(self, source_text, lineno, lexpos):
        self.init_object()
        self.source_text = source_text
        self.value = source_text
        self.lineno = lineno
        self.lexpos = lexpos

    def init_object(self):
        self.guid = str(uuid.uuid1())
        self.is_lexical_scope = False
        self.parent_lexical_scope_id = None  # 上一级的词法作用域ID
        self.has_lexical_scope = None  # 内部是否有词法作用域
        self.meta_scope_items = None  # 词法作用域中包括的key->value对
        self.meta_infos = {}  # 元素的元信息
        self.meta_proxy = None  # 元信息的代理，表示这个元素是另一个元素的元信息带来，获取self的元信息就是获取self.meta_proxy的元信息
        self.analysed_count = 0  # 元素被实际推导的次数
        self.max_meta_info_count = 100  # 元素最多包含的元信息数量，超过就抛弃部分可靠度最低的

    def get_meta_infos(self):
        if self.meta_proxy is not None:
            return self.meta_proxy.get_meta_infos()
        else:
            return self.meta_infos

    def remove_behind_meta_infos(self):
        """
        清理部分可靠度最低的元信息，暂时定为清理一半
        """
        # TODO

    def find_under_lexical_scopes(self):
        """
        找到自身或者内部全部词法作用域
        """
        return None

    @property
    def sub_items(self):
        """
        如果此元素内部有嵌套元素，返回它们的列表，
        否则返回None
        """
        return None

    @property
    def useful_items(self):
        """
        对语法推导可能有意义的嵌套子元素，
        如果没有返回None
        """
        return None

    def analyse(self, context):
        """
        分析单个元素，不包括它的子元素，但把子元素，如果有的话，放入推导队列中
        """
        self.analysed_count += 1

    def add_type_meta_info(self, info):
        # TODO: 如果类型信息已经完全固定下来了，比如基本类型，某些函数定义等，则添加类型元信息无效
        old_type = self.meta_infos.get('type')
        if old_type is None:
            self.meta_infos['type'] = info
        else:
            self.meta_infos['type'] = GOptionalType([old_type, info])

    def get_scope_env(self):
        """
        词法作用域才有，获取此词法作用域中的全部key->value对（不包括子词法作用域）
        """
        if self.meta_scope_items is None:
            self.meta_scope_items = dict()
        return self.meta_scope_items

    def add_scope_env_item(self, name, item):
        """
        词法作用域才有，把一个key->value对加入此词法作用域中
        """
        self.get_scope_env()[name] = item
        # print("the scope added %s=>%s" % (name, item))

    def get_scope_env_item(self, name):
        """
        从词法作用域中查找name对应的值，如果没有返回None
        """
        env = self.get_scope_env()
        return env.get(name, None)

    def get_scope_or_up_env_item(self, context, name):
        """
        从词法作用域或者更上层词法作用域中查找name对应的值，如果没有返回None
        """
        val = self.get_scope_env_item(name)
        if val is None:
            parent_scope = self.get_parent_lexical_scope(context)
            if parent_scope is None:
                return None
            else:
                return parent_scope.get_scope_or_up_env_item(context, name)
        else:
            return val

    def get_type_meta_info(self):
        mi = self.get_meta_infos()
        return mi.get('type', None)

    def lexical_scope_analyse(self, context):
        """
        如果是词法作用域的默认处理方式
        """
        context.add_to_derive_items(self.useful_items)

    def get_parent_lexical_scope(self, context, needle=None):
        """
        获取满足要求needle的上级的词法作用域，
        needle是词法作用域需要满足的条件，为空即是没有条件
        """
        scope_guid = self.parent_lexical_scope_id
        if scope_guid is None:
            return None
        elif needle is None:
            return context.get_lexical_scope_by_id(scope_guid)
        else:
            scope = context.get_lexical_scope_by_id(scope_guid)
            if needle(context, scope):
                return scope
            else:
                return scope.get_parent_lexical_scope(context, needle)

    def __repr__(self):
        return "<%s[%s]>" % (self.type, self.value)

    def to_json(self):
        """
        转成JSON，方便序列化传输
        """
        data = dict()
        if hasattr(self, 'lineno') and hasattr(self, 'lexpos'):
            data['lineno'] = self.lineno
            data['lexpos'] = self.lexpos
        if hasattr(self, 'source_text'):
            data['source_text'] = self.source_text
        if hasattr(self, 'get_definition_names'):
            data['definition_names'] = self.get_definition_names()
        if type(self.value) == int or type(self.value) == str or type(self.value) == bool:
            data['value'] = self.value
        if hasattr(self.value, 'to_json'):
            data['value'] = self.value.to_json()
        if hasattr(self, 'items'):
            data['items'] = map(lambda x: x.to_json(), self.items)
        return data