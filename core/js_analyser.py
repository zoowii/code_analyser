# coding: UTF-8
from __future__ import print_function
from collections import deque
from parser.js_types import JsProgram


class JsContext(object):
    """
    JavaScript的语法推导作用域，一个作用域可以作用在多个js文件（程序）中推导
    """

    def __init__(self):
        """
        初始化推导环境，比如导入js内置对象，设置等
        """
        self.current_loop_count = 0
        self.current_program = None
        self.current_program_name = None
        self.current_lexical_scopes = None
        self.derive_queue = None  # 推导队列
        self.init_analyse_env()
        self.max_analyse_count = 1000  # 100000  # 最多进行推导的次数（即最多放入元素到推导队列中多少次），为了测试使用较小的数
        self.current_analyse_count = 0  # 当前进行的推导的次数

    def init_analyse_env(self):
        """
        初始化推导环境
        """

    def collect_lexical_scopes(self, outer):
        """
        搜集词法作用域
        """
        if outer == self.current_program and isinstance(outer, JsProgram):
            self.current_lexical_scopes.append(outer)
            self.current_lexical_scopes_mapping[outer.guid] = outer

        current = outer
        current_items = current.useful_items
        if current_items is None:
            return
        for item in current_items:
            if item.is_lexical_scope and not (item in self.current_lexical_scopes):
                """
                如果是词法作用域，给词法作用域的所有内容（如果items存在的话），都添加指向此词法作用域的引用（或者是ID）
                """
                self.current_lexical_scopes.append(item)
                self.current_lexical_scopes_mapping[item.guid] = item
                if hasattr(item, 'items'):
                    for i in item.items:
                        i.parent_lexical_scope_id = item.guid
        for item in current_items:
            self.collect_lexical_scopes(item)

    def get_lexical_scope_by_id(self, guid):
        return self.current_lexical_scopes_mapping.get(guid, None)

    def add_to_derive_items(self, items):
        """
        向推导队列中增加待推导项
        """
        if items is None:
            return
        self.derive_queue.extend(items)

    def analyse_program(self, program, name=None):
        """
        program是语法分析后的JavaScript程序，
        name是要推导的程序的名称或者相对路径或绝对路径
        """
        self.current_program = program
        self.current_program_name = name
        self.current_lexical_scopes = []
        self.current_lexical_scopes_mapping = dict()
        self.derive_queue = deque()
        self.current_analyse_count = 0
        self.collect_lexical_scopes(self.current_program)
        print('collect lexical scopes done, there are %d lexical scopes' % len(self.current_lexical_scopes))
        self.derive_queue.append(self.current_program)  # 最开始推导队列中只有当前js程序本身
        self.process_analyse()

    def process_analyse(self):
        """
        推导的实际逻辑，从这里开始推导
        过程：按推导队列中的顺序不断取出元素进行分析，取出不放回（除非后面分析过程中又重新把此元素放入推导队列中）
        """
        while not self.end_of_analyse():
            self.current_analyse_count += 1
            element = self.derive_queue.popleft()
            # TODO
            element.analyse(self)
            # print(str(element)[:100])

    def end_of_analyse(self):
        """
        推导结束的条件判定:
* 错误
* 推导队列为空
* 循环或者推导次数达到一定数量，比如10万/100万
* 连续若干次对元素的推导（比如100次）都没有发生足够的变化（足够的变化的定义可以定义，比如元信息变化量超过5%），且推导队列中所有元素都至少被推导过一次，则停止推导
* 连续很多次对元素的推导（比如200次，500次）都没有发生足够的变化，则停止推导
* 元信息足够丰富，每个identifier(或者大部分，视已经运行时间/次数而定)都有足够可靠的元信息（比如被推导出来的次数>3）
        """
        if len(self.derive_queue) <= 0:
            return True
            # TODO!!!!!!!!!!!
        return self.current_analyse_count > self.max_analyse_count  # TODO: 当前只按推导次数来判定结束

    def after_analyse(self):
        """
        推导完成后的清理
        """
        print("TODO: after analyse")