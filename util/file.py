# coding: UTF-8
from __future__ import print_function


def readfile(filepath):
    f = open(filepath)
    return f.read()