# coding: UTF-8
from __future__ import print_function
import os
from flask import Flask, render_template, request, send_file
from flask_sockets import Sockets
import json
from parser.js2 import run_yacc
from core.js_analyser import JsContext

root_path = os.path.dirname(os.path.dirname(__file__))
app = Flask(__name__,
            static_folder=os.path.join(root_path, 'static'),
            template_folder=os.path.join(root_path, 'templates'))
sockets = Sockets(app)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/webide/load_file')
def load_webide_file():
    filename = request.args.get('filename')
    filepath = os.path.join(root_path, 'static/webide_demo/%s' % filename)
    f = open(filepath)
    data = f.read()
    f.close()
    return data


@sockets.route('/socket')
def websocket_connection(ws):
    while True:
        message = ws.receive()
        data = json.loads(message)
        if data['event'] == 'send_code_to_parse':
            code = data['data']
            request_id = data['id']
            code_anylised = run_yacc(code)
            context = JsContext()
            context.analyse_program(code_anylised, 'tmp.js')
            # print(code_anylised)
            data_res = json.dumps({
                'id': request_id,
                'event': 'code_parsed',
                'data': code_anylised.to_json()
            })
            ws.send(data_res)



